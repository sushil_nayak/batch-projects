package com.nayak.batch;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.util.function.Consumer;

@Slf4j
@RequiredArgsConstructor
public class WriterStep implements Consumer<String> {

    private final Connection connection;

    @Override
    public void accept(String s) {
        System.out.println("string now is " + s);
    }
}