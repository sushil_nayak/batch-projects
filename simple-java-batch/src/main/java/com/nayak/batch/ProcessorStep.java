package com.nayak.batch;

import lombok.RequiredArgsConstructor;

import java.util.function.Function;

@RequiredArgsConstructor
public class ProcessorStep implements Function<String, String> {

    @Override
    public String apply(String s) {
        return s.toUpperCase();
    }
}
