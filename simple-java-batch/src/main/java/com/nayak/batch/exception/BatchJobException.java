package com.nayak.batch.exception;

public class BatchJobException extends RuntimeException {

    public BatchJobException(String message, Throwable cause) {
        super(message, cause);
    }
}
