package com.nayak.batch;

import com.nayak.batch.exception.BatchJobException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Slf4j
@RequiredArgsConstructor
public class Job implements Runnable {

    public static final String BATCH_CORRELATION_ID = "batchCorrelationId";

    private final DataSource dataSource;

    @Override
    public void run() {
        MDC.put(BATCH_CORRELATION_ID, UUID.randomUUID().toString());

        doInJdbc(dataSource);

        MDC.remove(BATCH_CORRELATION_ID);
    }

    public void doInJdbc(DataSource dataSource) {
        log.info("Initiating Batch Job");

        try (Connection connection = dataSource.getConnection()) {

            connection.setAutoCommit(false);

            CompletableFuture<Void> jobCompletableFuture = CompletableFuture
                    .supplyAsync(new ReaderStep(connection))
                    .thenApply(new ProcessorStep())
                    .thenAccept(new WriterStep(connection));

            jobCompletableFuture.join();

            connection.commit();

        } catch (SQLException e) {
            log.error("DB_EXCEPTION: There was a problem with the DB", e);
            throw new BatchJobException("Connection Failure", e);
        }
    }

}
