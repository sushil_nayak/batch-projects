package com.nayak.exception;

public class BatchInitializationException extends RuntimeException {

    public BatchInitializationException(Throwable cause) {
        super(cause);
    }
}
