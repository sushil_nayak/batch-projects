package com.nayak;

import com.nayak.batch.Job;
import com.nayak.config.DataSourceConfig;
import com.nayak.config.JobConfig;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class BlockingBatchApp {

    public static void main(String[] args) throws IOException {
//        Environment environment = new Environment();
//
//        System.out.println(environment.getJdbcUrl());
//        System.out.println(System.getenv("JASYPT_ENCRYPTER_PASSWORD"));
        DataSource dataSource = new DataSourceConfig(new JobConfig()).dataSource();

        runBatchJob(dataSource);
    }

    public static void runBatchJob(DataSource dataSource) {

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());

        scheduledExecutorService.scheduleWithFixedDelay(new Job(dataSource), 1, 4, TimeUnit.SECONDS);
    }


}
