package com.nayak.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;

import javax.sql.DataSource;

@RequiredArgsConstructor
public class DataSourceConfig {

    private final JobConfig jobConfig;

    public DataSource dataSource() {

        HikariConfig hikariConfig = new HikariConfig();
//        hikariConfig.setJdbcUrl(jobConfig.getJdbcUrl());


        return new HikariDataSource(hikariConfig);
    }
}
