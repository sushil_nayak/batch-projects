package com.nayak.config.env;

import com.nayak.exception.BatchInitializationException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Properties;

@Slf4j
public class Environment {

    private final Properties properties;

    public Environment() {
        properties = new Properties();
        try {
            properties.load(ClassLoader.getSystemResourceAsStream("application.properties"));
        } catch (IOException e) {
            log.error("There was a problem reading application.properties file", e);
            throw new BatchInitializationException(e);
        }
        if (!properties.getProperty("active.profiles").isEmpty()) {
            try {
                properties.load(ClassLoader.getSystemResourceAsStream("application-" + properties.getProperty("active.profiles") + ".properties"));
            } catch (IOException e) {
                log.error("There was a problem reading application-{env}.properties file", e);
                throw new BatchInitializationException(e);
            }
        }

    }

    public String getJdbcUrl() {
        return properties.getProperty("datasource.url");
    }


    public String getUsername() {
        return properties.getProperty("datasource.username");
    }


    public String getPassword() {
        properties.getProperty("datasource.password");


        return "";
    }


}
