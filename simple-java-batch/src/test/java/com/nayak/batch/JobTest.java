package com.nayak.batch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JobTest {

    @Mock
    DataSource dataSource;

    @Test
    void jobShouldFailWithBatchJobExceptionWhenConnectionFails() throws SQLException {

        when(dataSource.getConnection()).thenThrow(SQLException.class);

        Job job = new Job(dataSource);

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<?> submit = executorService.submit(job);

        ExecutionException executionException = Assertions.assertThrows(ExecutionException.class, submit::get);

        assertThat(executionException.getLocalizedMessage()).isEqualTo("com.nayak.batch.exception.BatchJobException: Connection Failure");
    }

}