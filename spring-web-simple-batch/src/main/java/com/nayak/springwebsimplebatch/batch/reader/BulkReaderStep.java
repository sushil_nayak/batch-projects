package com.nayak.springwebsimplebatch.batch.reader;

import com.nayak.springwebsimplebatch.model.DecisionConsolidatedData;
import com.nayak.springwebsimplebatch.repository.CreditCardRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Slf4j
@RequiredArgsConstructor
@Component
public class BulkReaderStep implements Function<Void, DecisionConsolidatedData> {

    private final CreditCardRepository creditCardRepository;

    @Override
    public DecisionConsolidatedData apply(Void unused) {
        return null;
    }
}
