package com.nayak.springwebsimplebatch.batch;

import com.nayak.springwebsimplebatch.batch.processor.ProcessorStep;
import com.nayak.springwebsimplebatch.batch.reader.BulkReaderStep;
import com.nayak.springwebsimplebatch.batch.reader.ReaderForDecisionUniqueId;
import com.nayak.springwebsimplebatch.batch.writer.WriterStep;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class Job {

    private final BulkReaderStep bulkReaderStep;
    private final ReaderForDecisionUniqueId readerForDecisionUniqueId;
    private final ProcessorStep processorStep;
    private final WriterStep writerStep;

    public Runnable triggerForUniqueDecisionId(String decisionUniqueId) {
        log.info("Thread name = {}", Thread.currentThread().getName());
        return () -> readerForDecisionUniqueId
                .andThen(processorStep)
                .andThen(writerStep)
                .apply(decisionUniqueId);
    }


    public Runnable triggerInBatchMode() {
        log.info("Thread name = {}", Thread.currentThread().getName());
        return () -> bulkReaderStep
                .andThen(processorStep)
                .andThen(writerStep)
                .apply(null);

    }
}
