package com.nayak.springwebsimplebatch.batch.reader;

import com.nayak.springwebsimplebatch.model.DecisionConsolidatedData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Slf4j
@Component
public class ReaderForDecisionUniqueId implements Function<String, DecisionConsolidatedData> {

    @Override
    public DecisionConsolidatedData apply(String s) {
        return null;
    }
}
