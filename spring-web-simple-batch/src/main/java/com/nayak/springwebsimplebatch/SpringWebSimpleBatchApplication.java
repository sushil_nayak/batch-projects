package com.nayak.springwebsimplebatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAsync
@EnableScheduling
@SpringBootApplication
public class SpringWebSimpleBatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebSimpleBatchApplication.class, args);
    }

}
