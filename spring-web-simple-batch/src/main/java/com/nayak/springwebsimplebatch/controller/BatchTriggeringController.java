package com.nayak.springwebsimplebatch.controller;

import com.nayak.springwebsimplebatch.service.BatchTriggeringService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping
@RequiredArgsConstructor
public class BatchTriggeringController {

    private final BatchTriggeringService batchTriggeringService;

    @PostMapping("/{decisionUniqueId}")
    public ResponseEntity<Void> triggerJob(@PathVariable("decisionUniqueId") String decisionUniqueId) {

        batchTriggeringService.triggerViaController(decisionUniqueId);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
