package com.nayak.springwebsimplebatch.repository;

import com.nayak.springwebsimplebatch.model.DecisionConsolidatedData;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@RequiredArgsConstructor
@Repository
public class CreditCardRepository {

    final String DECISION_IDS_NOT_PROCESSED_SQL = "SELECT * FROM XXXXX WHERE 1=1";
    final String ROWS_NOT_PROCESSED_SQL = "SELECT COUNT(0) FROM XXXXX WHERE 1=1";
    final String INSERT_DECISION_SQL = "INSERT INTO ";

    private final JdbcTemplate jdbcTemplate;

    public Integer countRowsToBeProcessed() {
        Integer result = jdbcTemplate.queryForObject(INSERT_DECISION_SQL, Integer.class);
        return result != null ? result : 0;
    }

    public List<DecisionConsolidatedData> findAllDataForProcessing() {
        return jdbcTemplate.query(DECISION_IDS_NOT_PROCESSED_SQL, new DecisionRowMapper());
    }

    public DecisionConsolidatedData findDataByDecisionUniqueId(String decisionUniqueId) {
        return null;
    }


    static class DecisionRowMapper implements RowMapper<DecisionConsolidatedData> {
        @Override
        public DecisionConsolidatedData mapRow(ResultSet rs, int rowNum) throws SQLException {
            return null;
        }
    }
}
