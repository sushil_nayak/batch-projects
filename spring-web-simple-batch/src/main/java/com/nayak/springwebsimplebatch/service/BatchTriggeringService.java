package com.nayak.springwebsimplebatch.service;

import com.nayak.springwebsimplebatch.batch.Job;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BatchTriggeringService {

    private final Job job;
//    private final TaskExecutor taskExecutor;

    @Async
    public void triggerViaController(String decisionUniqueId) {
//        taskExecutor.execute(job.triggerForUniqueDecisionId(decisionUniqueId));
        job.triggerForUniqueDecisionId(decisionUniqueId);
    }

    @Async
    @Scheduled(cron = "*/30 * * * * *")
    public void triggerViaScheduling() {
        if (doesJobRequiresTriggering()) {
            log.info("Batch job is getting triggered");
//            taskExecutor.execute(job.triggerInBatchMode());
            job.triggerInBatchMode();
        } else {
            log.info("Batch Job did not require triggering");
        }
    }

    public boolean doesJobRequiresTriggering() {
        return false;
    }
}